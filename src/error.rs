//! Error handling.

/// Errors used in this module.
///
/// Note: This enum cannot be exhaustively matched to allow future
/// extensions.
#[non_exhaustive]
#[derive(thiserror::Error, Debug)]
pub enum Error {
    /// The output is unchanged.
    ///
    /// Returns the new cache tag.  Use it to update the stored tag.
    /// This way, if a file is modified but the output is the same,
    /// you will store the new modification time.
    #[error("Output unchanged")]
    Unchanged(crate::models::CacheTag),

    /// Errors related to `gpgconf`.
    #[error("gpgconf: {0}")]
    GPGConf(String),

    /// The remote operation failed.
    #[error("Operation failed: {0}")]
    OperationFailed(String),

    /// The remote party violated the protocol.
    #[error("Protocol violation: {0}")]
    ProtocolError(String),
}

//#[cfg(windows)]
pub fn log_internal<T: AsRef<str>>(text: T) {
    let text = format!("{}: {}", chrono::offset::Utc::now(), text.as_ref());

    if cfg!(windows) {
        // Save messages to a log file in the current profile's
        // directory (.../.thunderbird/$PROFILE/octopus.log).
        //
        // This is a bit hairy, because the code needs to be
        // reentrant: to initialize the logger's file description, we
        // need the location of the current profile, but finding that
        // location also uses the logging functionality.
        //
        // To break this cycle, if the logger is locked, rather than
        // wait for the lock, we simply enqueue the message in a
        // channel.  Then when we actually have the lock, we first
        // print any messages queued in the channel and then print out
        // our own message.

        use std::fs::File;
        use std::io::Write;
        use std::ops::DerefMut;
        use std::sync::mpsc::channel;
        use std::sync::mpsc::Receiver;
        use std::sync::mpsc::Sender;
        use std::sync::Arc;
        use std::sync::Mutex;

        struct State {
            sender: Mutex<Sender<String>>,
            // If None, the file has not yet been opened.
            output: Mutex<Option<(Receiver<String>, Option<File>)>>,
        }

        lazy_static! {
            static ref LOGGER: Arc<State> = {
                let (sender, receiver) = channel();

                Arc::new(State {
                    sender: Mutex::new(sender),
                    output: Mutex::new(Some((receiver, None))),
                })
            };
        }

        let mut logged = false;
        if let Ok(mut guard) = LOGGER.output.try_lock() {
            // We got the lock.

            // If initialization fails, we set output to None.  But
            // since it is borrowed, we need to delay it.
            let mut kill = false;
            if let Some((receiver, ref mut ofd)) = guard.deref_mut() {
                if ofd.is_none() {
                    kill = true;
                }

                if let Some(fd) = ofd {
                    // First, drain the message queue.
                    while let Ok(text) = receiver.try_recv() {
                        let _ = writeln!(fd, "{}", text);
                    }
                    // Then print our own message.
                    let _ = writeln!(fd, "{}", text);
                    let _ = fd.flush();
                    logged = true;
                }
            }

            if kill {
                *guard = None;
            }
        } else {
            // Locked.  Enqueue the message for later.  If we can't
            // send, it means initialization failed so just ignore.
            if let Ok(_) = LOGGER.sender.lock().unwrap().send(text.clone()) {
                logged = true;
            }
        }

        if !logged {
            // Something went wrong.  Just send it to stderr, which
            // probably won't do anything on Windows, but if we are
            // debugging on another platform, that will be useful.
            eprintln!("{}", text);
        }
    } else {
        // Just write to stderr.
        eprintln!("{}", text);
    }
}

// Like eprintln!
macro_rules! log {
    ($dst:expr $(,)?) => (
        $crate::error::log_internal($dst)
    );
    ($dst:expr, $($arg:tt)*) => (
        $crate::error::log_internal(std::format!($dst, $($arg)*))
    );
}

macro_rules! gpg_store_function {
    ( $fn_name: path, $TRACE: expr ) => {
        #[allow(unused_macros)]
        macro_rules! _trace {
            ( $msg: expr ) => {
                if $TRACE {
                    log!("gpg-store: TRACE: {}: {}",
                         stringify!($fn_name), $msg);
                }
            };
        }

        // Currently, Rust doesn't support $( ... ) in a nested
        // macro's definition.  See:
        // https://users.rust-lang.org/t/nested-macros-issue/8348/2
        #[allow(unused_macros)]
        macro_rules! t {
            ( $fmt:expr ) =>
            { _trace!( $fmt) };
            ( $fmt:expr, $a:expr ) =>
            { _trace!( format!($fmt, $a)) };
            ( $fmt:expr, $a:expr, $b:expr ) =>
            { _trace!( format!($fmt, $a, $b)) };
            ( $fmt:expr, $a:expr, $b:expr, $c:expr ) =>
            { _trace!( format!($fmt, $a, $b, $c)) };
            ( $fmt:expr, $a:expr, $b:expr, $c:expr, $d:expr ) =>
            { _trace!( format!($fmt, $a, $b, $c, $d)) };
            ( $fmt:expr, $a:expr, $b:expr, $c:expr, $d:expr, $e:expr ) =>
            { _trace!( format!($fmt, $a, $b, $c, $d, $e)) };
            ( $fmt:expr, $a:expr, $b:expr, $c:expr, $d:expr, $e:expr, $f:expr ) =>
            { _trace!( format!($fmt, $a, $b, $c, $d, $e, $f)) };
            ( $fmt:expr, $a:expr, $b:expr, $c:expr, $d:expr, $e:expr, $f:expr, $g:expr ) =>
            { _trace!( format!($fmt, $a, $b, $c, $d, $e, $f, $g)) };
            ( $fmt:expr, $a:expr, $b:expr, $c:expr, $d:expr, $e:expr, $f:expr, $g:expr, $h:expr ) =>
            { _trace!( format!($fmt, $a, $b, $c, $d, $e, $f, $g, $h)) };
            ( $fmt:expr, $a:expr, $b:expr, $c:expr, $d:expr, $e:expr, $f:expr, $g:expr, $h:expr, $i:expr ) =>
            { _trace!( format!($fmt, $a, $b, $c, $d, $e, $f, $g, $h, $i)) };
            ( $fmt:expr, $a:expr, $b:expr, $c:expr, $d:expr, $e:expr, $f:expr, $g:expr, $h:expr, $i:expr, $j:expr ) =>
            { _trace!( format!($fmt, $a, $b, $c, $d, $e, $f, $g, $h, $i, $j)) };
            ( $fmt:expr, $a:expr, $b:expr, $c:expr, $d:expr, $e:expr, $f:expr, $g:expr, $h:expr, $i:expr, $j:expr, $k:expr ) =>
            { _trace!( format!($fmt, $a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k)) };
        }

        #[allow(unused_macros)]
        macro_rules! warn {
            // Currently, Rust doesn't support $( ... ) in a nested
            // macro's definition.  See:
            // https://users.rust-lang.org/t/nested-macros-issue/8348/2
            //
            //( $fmt: expr $(, $a: expr )* ) => {
            //    eprintln!(concat!("gpg-store: ",
            //                      stringify!($fn_name),
            //                      ": ", $fmt)
            //              $(, $a )*);
            //};
            ( $fmt: expr ) => {
                log!(concat!("gpg-store: ",
                                  stringify!($fn_name),
                                  ": ", $fmt));
            };
            ( $fmt: expr, $a: expr ) => {
                log!(concat!("gpg-store: ",
                             stringify!($fn_name),
                             ": ", $fmt),
                     $a);
            };
            ( $fmt: expr, $a: expr, $b: expr ) => {
                log!(concat!("gpg-store: ",
                             stringify!($fn_name),
                             ": ", $fmt),
                     $a, $b);
            };
            ( $fmt: expr, $a: expr, $b: expr, $c: expr ) => {
                log!(concat!("gpg-store: ",
                             stringify!($fn_name),
                             ": ", $fmt),
                     $a, $b, $c);
            };
        }

        t!("Entering function");
    };
}

macro_rules! global_warn {
    ( $fmt: expr $(, $a: expr )* ) => {
        log!(concat!("gpg-store: ", $fmt)
             $(, $a )*);
    };
}
