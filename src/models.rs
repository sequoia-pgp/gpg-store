//! Models.

#[derive(Debug, Clone)]
pub struct CacheTag {
    /// Best effort size and modification time of the keyrings and
    /// trust databases.
    pub(crate) metadata: Vec<(&'static str, u64, std::time::SystemTime)>,
    /// Hash digest of the gpg output.
    pub(crate) hash: Vec<u8>,
}
